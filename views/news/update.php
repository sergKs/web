<?php

/* @var $this \yii\web\View */
/* @var $model \app\models\News */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Изменить новость №' . $model->id;
?>

<h1><?= $this->title ?></h1>

<?php if (Yii::$app->session->hasFlash('success')): ?>

	<p class="alert alert-success">Всё успешно сохранено !</p>

<?php endif; ?>

<?php $form = \yii\widgets\ActiveForm::begin() ?>

	<?= $form->field($model, 'title')->textInput() ?>

	<?= $form->field($model, 'description')->widget(\vova07\imperavi\Widget::class, [
		'settings' => [
			'lang' => 'ru',
			'minHeight' => 200,
			'imageUpload' => Url::to(['/news/image-upload']),
		]
	]) ?>

	<?= $form->field($model, 'active')->checkbox() ?>

	<div class="form-group">
		<?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
	</div>

<?php \yii\widgets\ActiveForm::end() ?>
