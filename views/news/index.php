<?php

/* @var $models \app\models\News[] */

?>

<h1>Новости</h1>

<?php foreach ($models as $model): ?>

	<article>
		<h3><?= $model->title ?></h3>
		<p><?= $model->description ?></p>

		<?php if (!Yii::$app->user->isGuest): ?>

			<div>
				<?= \yii\helpers\Html::a('Редактировать', ['news/update', 'id' => $model->id]) ?>
			</div>

		<?php endif; ?>
	</article>

<?php endforeach; ?>

