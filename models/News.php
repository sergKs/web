<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $title Заголовок
 * @property string $description Описание
 * @property string $text Текст
 * @property int $categoryId Категория
 * @property string $image Изображение
 * @property string $createdAt Дата создания
 *
 * @property Image[] $images
 * @property Category $category
 */
class News extends \yii\db\ActiveRecord
{
	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'news';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['title', 'text'], 'required'],
			[['description', 'text'], 'string'],
			[['categoryId'], 'integer'],
			[['createdAt'], 'safe'],
			[['title'], 'string', 'max' => 255],
			[['image'], 'string', 'max' => 256]];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'title' => 'Заголовок',
			'description' => 'Описание',
			'text' => 'Текст',
			'categoryId' => 'Категория',
			'image' => 'Изображение',
			'createdAt' => 'Дата создания',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getImages()
	{
		return $this->hasMany(Image::className(), ['newsId' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCategory()
	{
		return $this->hasOne(Category::className(), ['id' => 'categoryId']);
	}
}