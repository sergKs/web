<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "images".
 *
 * @property int $id
 * @property int $newsId
 * @property string $filename
 *
 * @property News $news
 */
class Image extends \yii\db\ActiveRecord
{
	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'images';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['newsId', 'filename'], 'required'],
			[['newsId'], 'integer'],
			[['filename'], 'string', 'max' => 256],
			[['newsId'], 'exist', 'skipOnError' => true, 'targetClass' => News::className(), 'targetAttribute' => ['newsId' => 'id']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'newsId' => 'News ID',
			'filename' => 'Filename',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getNews()
	{
		return $this->hasOne(News::className(), ['id' => 'newsId']);
	}
}