<?php

namespace app\controllers;

use app\models\News;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class NewsController extends Controller
{
	public function actions()
	{
		$path = 'uploads/' . date('Y/m/d');
		FileHelper::createDirectory($path);

		return [
			'image-upload' => [
				'class' => 'vova07\imperavi\actions\UploadFileAction',
				'url' => '/' . $path, // Directory URL address, where files are stored.
				'path' => $path, // Or absolute path to directory where files are stored.
			],
		];
	}


	public function actionIndex()
	{
		$models = News::find()->all();

		return $this->render('index', [
			'models' => $models
		]);
	}

	/**
	 * @param $id
	 * @return string
	 * @throws NotFoundHttpException
	 */
	public function actionUpdate($id)
	{
		$model = News::findOne(['id' => $id]);

		if ($model === null) {
			throw new NotFoundHttpException('Новость не найдена.');
		}

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash('success');
			return $this->refresh();
		}

		return $this->render('update', [
			'model' => $model
		]);
	}

	public function actionFileUpload()
	{

	}
}